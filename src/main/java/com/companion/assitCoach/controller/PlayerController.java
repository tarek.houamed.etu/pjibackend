package com.companion.assitCoach.controller;


import com.companion.assitCoach.model.Player;
import com.companion.assitCoach.service.PlayerService;
import com.companion.assitCoach.service.TeamService;
import lombok.AllArgsConstructor;

import org.springframework.web.bind.annotation.*;



import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/player")
@AllArgsConstructor
@CrossOrigin
public class PlayerController {

    private final PlayerService playerService;
    private final TeamService teamService;

    @GetMapping("/all")
    public List<Player> getAllPlayers() {
        return playerService.getAllPlayers();
    }

    @GetMapping("/{id}")
    public Optional<Player> findPlayer(@PathVariable String id) {
        return playerService.getPlayerById(id);
    }

    @GetMapping("/findByNumber")
    public Optional<Player> findPlayerByNumber(@RequestParam Integer number) {
        return playerService.getPlayerByNumber(number);
    }

    @GetMapping("/findByTeam/{team}")
    public Optional<List<Player>> findByTeam(@PathVariable String team) {
        return playerService.getPlayersByTeam(team);
    }

    @GetMapping("/findlastname")
    public Optional<Player> findPlayerByLastName(@RequestParam String lastname) {
        return playerService.getPlayerByLastName(lastname);
    }


    @GetMapping("/teamname/{teamName}")
    public Optional<List<Player>> findPlayersByTeamName(@PathVariable String teamName){
        return playerService.getPlayersByTeam(teamName);
    }

    @GetMapping("findallbylastname")
    public Optional<List<Player>> findAllByLastName(@RequestParam String lastname) {
        return playerService.getAllPlayersByLastName(lastname);
    }

    @PostMapping("/add")
    public String addPlayer(@RequestBody Player player) {

        playerService.addPlayer(player);
        teamService.addPlayerInTeam(player.getTeam(),player.getId());
        System.out.println(player.getTeam());
        System.out.println(player.getId());
        return "new player added";
    }




    @PutMapping("/update/{id}")
    public String updatePlayer(@RequestBody Player player) {
        playerService.updatePlayer(player);
        return "player updated";
    }

    @DeleteMapping("/{id}")
    public String deletePlayer(@PathVariable String id) {
        playerService.deletePlayer(id);
        return "player deleted";
    }



}
