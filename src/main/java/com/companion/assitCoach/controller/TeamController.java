package com.companion.assitCoach.controller;

import com.companion.assitCoach.model.Player;
import com.companion.assitCoach.model.Team;
import com.companion.assitCoach.service.TeamService;
import lombok.AllArgsConstructor;


import org.springframework.web.bind.annotation.*;



import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/team")
@AllArgsConstructor//Sinon on doit faire un constructeur avec tous les arguments
@CrossOrigin
public class TeamController {

    private final TeamService teamService;

    @GetMapping("/all")
    public List<Team> getAllTeams() {
        return teamService.getAllTeams();
    }

    @GetMapping("/{id}")
    public Optional<Team> getTeam(@PathVariable String id){
        return teamService.getTeamById(id);
    }

    @PostMapping("/add")
    public String addTeam(@RequestBody Team team) {
        teamService.createTeam(team);
        return "Team added";
    }


    @DeleteMapping("/{teamId}")
    public String deleteTeam(@PathVariable String teamId) {
        teamService.deleteTeam(teamId);
        return "Team deleted";
    }

    @PutMapping("/update")
    public String updateTeam(@RequestBody Team team) {
        teamService.updateTeam(team);
        return "Team updated";
    }

    @PutMapping("/addPlayer/{teamName}")
    public String addPlayer(@RequestBody Player player,@PathVariable String teamName) {
        String playerId = player.getId();
        System.out.println(playerId);
        teamService.addPlayerInTeam(teamName, playerId);
        return "Player added";
    }

    @DeleteMapping("/deletePlayer/{teamName}")
    public String deletePlayer(@RequestParam String playerId, @PathVariable String teamName) {
        teamService.removePlayerFromTeam(teamName, playerId);
        return "Player deleted";
    }




    @GetMapping("/name/{name}")
    public Team getTeamByName(@PathVariable String name) {
        return teamService.getTeamByName(name);
    }




}
