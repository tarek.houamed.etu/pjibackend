package com.companion.assitCoach.repository;

import com.companion.assitCoach.model.Player;
import com.companion.assitCoach.model.Team;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.math.BigInteger;
import java.util.Optional;

public interface teamRepository extends MongoRepository<Team, String> {


    Optional<Team> deleteByTeamName(String name);


    Team findByTeamName(String name);

    Optional<Team> findById(String id);


}
