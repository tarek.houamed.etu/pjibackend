package com.companion.assitCoach.service;


import com.companion.assitCoach.model.Team;
import com.companion.assitCoach.repository.teamRepository;
import lombok.AllArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.mongodb.core.query.Criteria;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.core.*;


@AllArgsConstructor
@Service
public class TeamService {
    @Autowired
    MongoTemplate mongoTemplate;
    private final teamRepository TeamRepository;

    public List<Team> getAllTeams() {
        return TeamRepository.findAll();
    }

    public void createTeam(Team team) {
        TeamRepository.save(team);
    }

    public void deleteTeam(String id) {
        TeamRepository.deleteById(id);
    }


    public void addPlayerInTeam(String teamName, String playerId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("teamName").is(teamName));
        Update update = new Update();
        update.addToSet("playersId", playerId);
        mongoTemplate.updateFirst(query, update, Team.class);
    }

    public void removePlayerFromTeam(String teamName, String playerId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("teamName").is(teamName));
        Update update = new Update();
        update.pull("playersId", playerId);
        mongoTemplate.updateFirst(query, update, Team.class);
    }
    public Optional<Team> getTeamById(String id) {
        return  TeamRepository.findById(id);
    }


    public Team getTeamByName(String name) {
        return TeamRepository.findByTeamName(name);
    }


    public void updateTeam(Team team) {
        TeamRepository.save(team);
    }
}
